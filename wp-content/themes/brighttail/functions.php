<?php
// =========================================================================
// BRIGHTTAIL FUNCTIONS AND DEFINITIONS
// =========================================================================
 
 if ( ! function_exists( 'brighttail_setup' ) ) :
	function brighttail_setup() {
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'brighttail' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
			'header-text' => array( 'site-title', 'site-description' )
		) );
	}
endif;

add_action( 'after_setup_theme', 'brighttail_setup' );

/**
 *  Register custom post type Work
 */ 
   
add_action( 'init', function() {
   $work_labels = array(
      'name'               => _x('Work', 'post type general name'),
      'singular_name'      => _x('Work', 'post type singular name'),
      'add_new'            => _x('Add New', 'Work'),
      'add_new_item'       => __('Add New Work'),
      'edit_item'          => __('Edit Work'),
      'new_item'           => __('New Work'),
      'view_item'          => __('View Work'),
      'all_items'          => __('All Work'),
      'search_items'       => __('Search Work'),
      'not_found'          => __('No work found'),
      'not_found_in_trash' => __('No work found in Trash'),
      'parent_item_colon'  => ''
   );
   
   $work_args = array(
      'labels'             => $work_labels,
      'public'             => true,
      'public_queryable'   => true,
      'rewrite'            => array(
         "slug" => "work"
      ),
      'menu_position'      => 5,
      'supports'           => array('title', 'thumbnail', 'editor'),
      'menu_icon'          => 'dashicons-portfolio',
      'has_archive'        => false
   );
   
   register_post_type( 'work', $work_args );
});

/**
 *  Register widget area.
 */ 
 
function brighttail_widgets_init() {
	register_sidebar( array(
		'name'          => 'Social Media',
		'id'            => 'footer-social-media',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	register_sidebar( array(
		'name'          => 'Single Work Common Content',
		'id'            => 'single-work-common-content',
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}

add_action( 'widgets_init', 'brighttail_widgets_init' );

/**
 *  Register and load the widget
 */ 
function register_work_widgets() {
   register_widget( 'work_section_titles_widget' );
}

add_action( 'widgets_init', 'register_work_widgets' );
 
if ( !class_exists('work_section_titles_widget') ) {
   class work_section_titles_widget extends WP_Widget {       
      public function __construct() {
         $widget_ops = array(
            'classname' => 'work_section_titles_widget',
            'description' => 'Section titles widget for Work',
         );
         parent::__construct( 'work_section_titles_widget', 'Work Section Titles', $widget_ops );
      }
      
      // output the widget content on the front-end
      public function widget( $args, $instance ) {}

   	// output the option form field in admin Widgets screen
   	public function form( $instance ) {}
   
   	// save options
   	public function update( $new_instance, $old_instance ) {}
   }
}

/**
 *  Allow upload of SVG files into Media Library.   
 */   
   
function cc_mime_types( $mimes ) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}

add_filter ( 'upload_mimes', 'cc_mime_types' );

/**
 * Enqueue scripts and styles.
 */
 
function brighttail_scripts() {
	wp_enqueue_style( 'brighttail-style', get_stylesheet_uri(), array(), null );
	wp_enqueue_script( 'vendor', get_stylesheet_directory_uri() . '/production/js/vendor.min.js', array( 'jquery' ), null, true ); 
   wp_enqueue_script( 'script', get_stylesheet_directory_uri() . '/production/js/script.min.js', array( 'jquery' ), null, true );   
}

add_action( 'wp_enqueue_scripts', 'brighttail_scripts' );


// =========================================================================
// REMOVE JUNK FROM HEAD
// =========================================================================
remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
remove_action('wp_head', 'wp_generator'); // remove wordpress version

remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links

remove_action('wp_head', 'index_rel_link'); // remove link to index page
remove_action('wp_head', 'wlwmanifest_link'); // remove wlwmanifest.xml (needed to support windows live writer)

remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );

remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0 );

remove_action('wp_head', 'print_emoji_detection_script', 7);
remove_action('wp_print_styles', 'print_emoji_styles');

remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );
