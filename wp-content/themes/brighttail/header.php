<!DOCTYPE html>
<html <?php language_attributes(); ?>>
   <head>
   	<meta charset="<?php bloginfo( 'charset' ); ?>">
   	<meta http-equiv="X-UA-Compatible" content="IE=edge">
   	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=0">
   	
      <link href="https://fonts.googleapis.com/css?family=Rajdhani:300,500,700|Source+Sans+Pro:300,300i,400,400i,600i,700,700i,900" rel="stylesheet">
      
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdb.com/html5shiv/3.7.2/html5shiv.min.js"></script>
         <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
      <![endif]-->
      
   	<?php wp_head(); ?>
   </head>
   <body <?php body_class(); ?>>
      <header>
         <?php the_custom_logo(); ?>
         <input id="burger-menu" type="checkbox" />
         <label for="burger-menu">
            <span></span>
            <span></span>
            <span></span>
         </label>
         <?php
   			wp_nav_menu( array(
   				'theme_location' => 'menu-1',
   				'menu_id'        => 'primary-menu',
   				'container'      => ''
   			) );
			?>
      </header>
      <main>