<?php
/*
   Template Name: Thought Leadership  
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="image-container">
               <div id="moon" class="image"></div>
               <div id="satelite" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-11 col-sm-8 col-md-7 offset-md-right-4 col-lg-5 offset-lg-right-6">
                     <div class="headlines">
                        <h1><?= get_field('landing_headline'); ?></h1>
                        <p><?= get_field('landing_subheadline'); ?></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <section id="intro">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-9 offset-md-right-2 col-lg-8 offset-lg-right-3 col-xl-7 offset-xl-right-4">
               <div class="headline">
                  <h2><?= get_field('intro_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-8 col-lg-6 offset-lg-right-2 col-xl-5 offset-xl-right-3">
               <div class="text-container">
                  <?= get_field('intro_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="info">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-11">
               <div class="headline">
                  <h2><?= get_field('info_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-12 col-lg-11 col-xl-10">
            <?php if (have_rows('statistics')): ?>
               <ul id="statistics">
               <?php while (have_rows('statistics')): the_row(); ?><li>
                  <div class="percentage-chart" data-percentage="<?= get_sub_field('percentage'); ?>">
                     <svg viewBox="0 0 36 36" class="circular-chart">
                        <path class="circle-bg" d="M18 2.0845
                         a 15.9155 15.9155 0 0 1 0 31.831
                         a 15.9155 15.9155 0 0 1 0 -31.831" />
                        <path class="circle" stroke-dasharray="0, 100" d="M18 2.0845
                         a 15.9155 15.9155 0 0 1 0 31.831
                         a 15.9155 15.9155 0 0 1 0 -31.831" />
                     </svg>
                     <label><span class="number"><?= get_sub_field('percentage'); ?></span><small>%</small></label>
                  </div>
                  <p><?= get_sub_field('fact'); ?></p>
                  </li><?php endwhile; ?>
               </ul>
            <?php endif; ?>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-11">
               <p id="credit"><?= get_field('credit'); ?></p>
            </div>
         </div>
      </div>
   </section>
   <section id="pitch">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-9 offset-md-right-2 col-lg-8 col-xl-7 offset-xl-right-3">
               <div class="headline">
                  <h2 class="case-normal"><?= get_field('pitch_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-8 col-lg-6 offset-lg-2 col-xl-5">
               <div class="text-container">
                  <?= get_field('pitch_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="clients">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-9 offset-md-right-2 col-lg-8 offset-lg-right-3 col-xl-7 offset-xl-right-4">
               <div class="headline">
                  <h2><?= get_field('clients_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-11">
            <?php if (have_rows('clients')): 
               $size = count(get_field('clients'));
            ?>
               <div id="clients-list">
               <?php while (have_rows('clients')): the_row(); 
                  $post = get_sub_field('client');
                  setup_postdata($post);
               ?>
                  <div>
                     <a href="<?php the_permalink(); ?>">
                        <div class="cover" style="background-image:url(<?= get_sub_field('cover_image')['sizes']['large']; ?>)"></div>
                        <div class="text-container">
                           <h4><?php the_title(); ?></h4>
                           <p><?php the_field('headline'); ?></p>
                        </div>
                     </a>
                  </div>
               <?php wp_reset_postdata(); endwhile; ?>
               
               <?php 
                  if ($size <= 3):
                     while (have_rows('clients')): the_row(); 
                     $post = get_sub_field('client');
                     setup_postdata($post);
               ?>
                  <div>
                     <a href="<?php the_permalink(); ?>">
                        <div class="cover" style="background-image:url(<?= get_sub_field('cover_image')['sizes']['large']; ?>)"></div>
                        <div class="text-container">
                           <h4><?php the_title(); ?></h4>
                           <p><?php the_field('headline'); ?></p>
                        </div>
                     </a>
                  </div>
               <?php    wp_reset_postdata(); 
                     endwhile; 
                  endif; ?>
               </div>
            <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <div class="headline">
                  <h2><?php the_field('contact_headline'); ?></h2>
               </div>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>