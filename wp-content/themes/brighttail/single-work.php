<?php get_header(); while ( have_posts() ) : the_post(); ?>
<div class="wrapper <?php if (get_field('results_section')) { echo 'with-results'; } ?>">
    <div class="page-intro">
      <div class="background">
         <div class="content desktop" style="background-image:url(<?= get_the_post_thumbnail_url(get_the_ID(), 'large'); ?>)">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-11 col-sm-10 col-md-9 col-lg-7 col-xl-6 <?php if (get_field('text_alignment') == 'right') { echo 'right offset-md-2 offset-lg-5'; } else { echo 'offset-md-right-2 offset-lg-right-5'; } ?>">
                     <div class="headline">
                        <div class="logo">
                           <img src="<?= get_field('logo')['sizes']['medium']; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
                        </div>
                        <h2><?= get_field('headline'); ?></h2>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      <?php if (get_field('cover_image_mobile')): ?>
         <div class="content mobile" style="background-image:url(<?= get_field('mobile_cover_image')['sizes']['large']; ?>)">
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-11 col-sm-10 col-md-9 col-lg-7 col-xl-6 <?php if (get_field('text_alignment') == 'right') { echo 'right offset-md-2 offset-lg-5'; } else { echo 'offset-md-right-2 offset-lg-right-5'; } ?>">
                     <div class="headline">
                        <div class="logo">
                           <img src="<?= get_field('logo')['sizes']['medium']; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
                        </div>
                        <h2><?= get_field('headline'); ?></h2>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      <?php endif; ?>
      </div>
   </div>
   <section id="back-story">
   <?php if (get_field('results_section')): ?>
      <div class="background inverted"></div>
   <?php endif; ?>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-9 col-lg-7 col-xl-6 <?= (get_field('results_section') ? 'offset-md-right-2 offset-lg-right-4 offset-xl-right-5' : 'offset-md-2 offset-lg-4 offset-xl-5'); ?>">
               <h2 class="section-title"><?php the_field('title_back_story', 'widget_work_section_titles_widget-5'); ?></h2>
               <div class="text-container">
                  <?= get_field('content_back_story'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="solutions" class="type-<?= get_field('showcase'); ?>">
   <?php if (get_field('results_section')): ?>
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-8 col-xl-7 offset-md-1 offset-lg-3 offset-xl-4">
               <h2 class="section-title"><?php the_field('title_solution', 'widget_work_section_titles_widget-5'); ?></h2>
               <div class="text-container">
                  <?= get_field('content_solution'); ?>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-12 col-md-11">
               <div class="showcase">
                  <div>
               <?php switch(get_field('showcase')): 
                  case 'mockup': ?>
                  <img class="mockup" src="<?= get_field('mockup')['sizes']['large']; ?>" title="<?php the_title(); ?>-mockup" alt="<?php the_title(); ?>-mockup" />
               <?php break; 
                  case 'screenshot': 
                     $screenshots = get_field('screenshots');
                     if ($screenshots):
                  ?>
                  <ul class="screenshots" data-size="<?= sizeof($screenshots); ?>">
                  <?php foreach( $screenshots as $screenshot ): ?><li>
                        <img src="<?= $screenshot['sizes']['large']; ?>" title="<?php the_title(); ?>-screenshot-<?= $i; ?>" alt="<?php the_title(); ?>-screenshot-<?= $i; ?>" />
                     </li><?php endforeach; ?>
                  </ul>
               <?php endif;
                     break; 
                  case 'video': 
                     $videoID = get_field('video'); 
                     if ($videoID):
                  ?>
                  <div class="video-container">
                     <iframe src="//www.youtube.com/embed/<?= $videoID; ?>?enablejsapi=1&html5=1" frameborder="0" allowfullscreen></iframe>
                  </div>
               <?php endif;
                     break;
                  endswitch; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <?php else: ?>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-12 col-md-11">
               <div class="showcase">
                  <div>
               <?php switch(get_field('showcase')): 
                  case 'mockup': ?>
                  <img class="mockup" src="<?= get_field('mockup')['sizes']['large']; ?>" />
               <?php break; 
                  case 'screenshot': 
                     $screenshots = get_field('screenshots');
                     if ($screenshots):
                  ?>
                  <ul class="screenshots" data-size="<?= sizeof($screenshots); ?>">
                  <?php foreach( $screenshots as $screenshot ): ?><li>
                        <img src="<?= $screenshot['sizes']['large']; ?>" title="<?php the_title(); ?>-screenshot-<?= $i; ?>" alt="<?php the_title(); ?>-screenshot-<?= $i; ?>" />
                     </li><?php endforeach; ?>
                  </ul>
               <?php endif;
                     break; 
                  case 'video': 
                     $videoID = get_field('video'); 
                     if ($videoID):
                  ?>
                  <div class="video-container">
                     <iframe src="//www.youtube.com/embed/<?= $videoID; ?>?enablejsapi=1&html5=1" frameborder="0" allowfullscreen></iframe>
                  </div>
               <?php endif;
                     break;
                  endswitch; ?>
                  </div>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-lg-8 col-xl-7 offset-md-right-1 offset-lg-right-3 offset-xl-right-4">
               <h2 class="section-title"><?php the_field('title_solution', 'widget_work_section_titles_widget-5'); ?></h2>
               <div class="text-container">
                  <?= get_field('content_solution'); ?>
               </div>
            </div>
         </div>
      </div>
   <?php endif; ?>
   </section>
<?php if (get_field('results_section')): ?>
   <section id="results">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-8 col-xl-7 offset-md-right-1 offset-lg-right-3 offset-xl-right-4">
               <h2 class="section-title"><?php the_field('title_results', 'widget_work_section_titles_widget-5'); ?></h2>
               <div class="text-container">
                  <?= get_field('content_results'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>
<?php if (get_field('testimonial_section')): ?>
   <section id="testimonial">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-9 col-xl-8">
               <h2 class="section-title"><?php the_field('title_testimonial', 'widget_work_section_titles_widget-5'); ?></h2>
               <blockquote><?= get_field('statement'); ?></blockquote>
               <div class="customer">
               <?php if (get_field('customer')): ?>
                  <div class="person">
                     <span class="name"><?= get_field('customer')['name'];  ?></span>
                     <span class="title"><?= get_field('customer')['title'];  ?></span>
                  </div>
               <?php endif; ?>
                  <div class="logo">
                     <div>
                        <img src="<?= get_field('logo')['sizes']['medium']; ?>" title="<?php the_title(); ?>" alt="<?php the_title(); ?>" />
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
<?php endif; ?>
   <section id="contact" class="<?php if (!get_field('testimonial_section')) { echo 'no-testimonial'; } ?>">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <h2 class="section-title"><?php the_field('title_contact', 'widget_work_section_titles_widget-5'); ?></h2>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php if (get_field('showcase') == 'screenshot'): ?>
<div class="overlay">
   <span class="close"></span>
   <div class="window">
   <?php $screenshots = get_field('screenshots');
      $i = 0;
      foreach( $screenshots as $screenshot ): 
         $i++;
      ?>
         <img src="<?= $screenshot['url']; ?>" title="<?php the_title(); ?>-screenshot-<?= $i; ?>" alt="<?php the_title(); ?>-screenshot-<?= $i; ?>" />
   <?php endforeach; ?>
   </div>
</div>
<?php endif; ?>
<?php endwhile; get_footer(); ?>