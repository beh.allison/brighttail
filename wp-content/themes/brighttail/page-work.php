<?php
/*
   Template Name: Work    
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <section id="work">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-11 col-xl-10">
               <h2><?php the_title(); ?></h2>
            </div>
            <div class="col col-11 col-sm-10 col-md-11 col-xl-10">
            <?php if (have_rows('work')): ?>
               <div class="works-container">
               <?php while (have_rows('work')): the_row(); 
                  $post = get_sub_field('page_link');
                  $image = get_sub_field('cover_image');
                  setup_postdata($post);
               ?><div class="work">
                     <div>
                        <div class="cover" style="background-image:url(<?= $image['sizes']['large']; ?>);"></div>
                        <div class="title-container">
                           <h3><?php the_title(); ?></h3>
                           <p><?php the_field('headline'); ?></p>
                           <a href="<?php the_permalink(); ?>" class="button">See more</a>
                        </div>
                     </div>
                  </div><?php wp_reset_postdata(); 
                  endwhile; ?>
               </div>
            <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <h2 class="section-title"><?php the_field('title_contact', 'widget_work_section_titles_widget-5'); ?></h2>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>