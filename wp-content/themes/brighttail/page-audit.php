<?php
/*
   Template Name: Audit   
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <section>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11">
               <h2><?php the_title(); ?></h2>
               <div id="cf7" class="form-container">
               <?= do_shortcode('[contact-form-7 id="429" title="Quiz"]'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>