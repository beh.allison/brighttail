<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'brighttail');

/** MySQL database username */
define('DB_USER', 'bt_wp_user');

/** MySQL database password */
define('DB_PASSWORD', 'YQfrlsRJkayFAJ4F');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'C( :A3GzbN7Ho*XL1vww>%GG8C,~1g.QDL,s t6}_/SDLZD/h^tFS7kI749]gaLi');
define('SECURE_AUTH_KEY',  '<.x~*714Ox|C&]k*/Pq R$|dWI)>7RMvLIVbzJR2f&D.+ -w%Adsp_u0+lLi,WPM');
define('LOGGED_IN_KEY',    '*yUp[s/dvzP0b-BP7|u:E0cXe$12B=2AWS#E:ui%^*=kK>8zOr:MB+lB[Jx!|PPE');
define('NONCE_KEY',        '3h9tL7JmsVt^y<s6ht*.Np-Jk e%5/?iipM}NO<w~KL,gNO@c&J]`( b2.>^5}F(');
define('AUTH_SALT',        '/N,i^$?%$q$WTt3W#SQtSP5wxd6k55B~`K5Z=@00HuTG}hl^?k.B&Xd73`|G4}ZC');
define('SECURE_AUTH_SALT', 'IAp9zJm!vIog u!hK8i]xW$VGQCnAA)}[rFY)AS)io% 0Po~HNu280_$n}J$Rhp&');
define('LOGGED_IN_SALT',   'B8(jMdQY!(@s1+34*L91NL6<?Bi{M_!9L^@rUax/yxQdP]nf5(PYmHa^IRKR[-cu');
define('NONCE_SALT',       'hVxXj/n27A;!4~(1[P0d(a6wPG$|^3-b$-uxeQbC<8,*Uw9JC)#dzc-4KZqDQrmA');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'BT_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
