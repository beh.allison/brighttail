<?php
/*
   Template Name: Services   
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="stars-container">
               <div id="small" class="stars"></div>
               <div id="medium" class="stars"></div>
               <div id="big" class="stars"></div>
            </div>
            <div class="image-container">
               <div id="planet" class="image"></div>
               <div id="satelite" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-9 offset-3 col-sm-8 offset-sm-4 col-md-6 offset-md-4 col-lg-5 offset-lg-5">
                     <div class="headlines">
                        <h1><?= get_field('landing_headline'); ?></h1>
                        <p><?= get_field('landing_subheadline'); ?></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <section id="services">
   <?php if (have_rows('services')):
      $i = 1;
      while (have_rows('services')): the_row(); ?>
      <div class="subsection">
         <div class="container">
            <div class="row justify-content-center">
               <div class="col col-10 <?= ($i % 2 == 0 ? 'offset-1 offset-sm-0' : 'offset-right-1' ); ?> col-sm-3 <?php if ($i == 1) { echo 'offset-sm-right-1 offset-md-right-3 offset-lg-right-5'; } elseif ($i == 3) { echo 'offset-sm-right-0'; } elseif ($i == 4) { echo 'offset-sm-1 offset-md-3 offset-lg-5'; } ?> col-lg-2">
                  <div class="image-container">
                     <img src="<?= get_sub_field('icon')['sizes']['medium']; ?>" title="<?= get_sub_field('service'); ?>" alt="<?= get_sub_field('service'); ?>" />
                  </div>
               </div>
               <div class="col col-10 <?= ($i % 2 == 0 ? 'offset-1 offset-sm-0' : 'offset-right-1' ); ?> col-sm-8 <?php if ($i == 1) { echo 'offset-sm-right-0'; } elseif ($i == 2) { echo 'offset-sm-1 offset-md-3 offset-lg-5'; } elseif ($i == 3) { echo 'offset-sm-right-1 offset-md-right-3 offset-lg-right-5'; } ?> col-md-6 col-lg-5 col-xl-4">
                  <div class="text-container">
                     <h4><?= get_sub_field('service'); ?></h4>
                     <div class="description"><?= get_sub_field('description'); ?></div>
                     <a href="<?= get_sub_field('page_link')['link']; ?>" class="button"><?= get_sub_field('page_link')['cta']; ?></a>
                  </div>
               </div>
            </div>
         </div>
      </div>
   <?php $i++; endwhile;
      endif; ?>
   </section>
   <section id="testimonial">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-9 col-xl-8">
               <h2 class="section-title"><?php the_field('title_testimonial', 'widget_work_section_titles_widget-5'); ?></h2>
               <blockquote><?= get_field('statement'); ?></blockquote>
               <?php if (get_field('customer')): ?>
               <div class="customer">
               
                  <div class="person">
                     <span class="name"><?= get_field('customer')['name'];  ?></span>
                     <span class="title"><?= get_field('customer')['title'];  ?></span>
                  </div>
                  <?php 
                     $company = get_field('customer')['company'];
                     $cta = get_field('cta'); ?>
                  <div class="logo">
                     <div>
                        <img src="<?= get_field('logo', $company->ID)['sizes']['medium']; ?>" title="<?= $company; ?>" alt="<?= $company; ?>"/>
                     </div>
                  </div>
               </div>
                <a href="<?= get_the_permalink($company->ID); ?>" class="button"><?= $cta; ?></a>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <h2 class="section-title"><?php the_field('contact_headline'); ?></h2>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>