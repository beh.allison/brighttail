<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="stars-container">
               <div id="small" class="stars"></div>
               <div id="medium" class="stars"></div>
               <div id="big" class="stars"></div>
            </div>
            <div class="image-container">
               <div id="moons" class="image"></div>
               <div id="land" class="image"></div>
               <div id="space-probe" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-10 offset-1 col-sm-8 offset-sm-4 col-md-7 col-lg-5 offset-lg-1 offset-lg-right-6">
                     <div class="headlines">
                        <h1>404 Error</h1>
                        <p>Oops, it seems that we’ve crash landed.</p>
                        <a href="<?= site_url(); ?>" class="button">Go home</a>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<?php get_footer(); ?>