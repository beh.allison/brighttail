         <footer>
            <div class="container">
               <div class="row">
                  <div class="col col-12 secondary">
                     <?php if( is_active_sidebar('footer-social-media') ) {
                        dynamic_sidebar('footer-social-media');
                     } ?>
                  </div>
                  <div class="col col-12 col-md-12">
                     <p id="copyright">&copy; <?= date('Y') . ' '; bloginfo('name'); echo ', Inc. All Rights Reserved.'; ?></p>
                  </div>
               </div>
            </div>
         </footer>
      </main>
      <?php wp_footer(); ?>
   </body>
</html>