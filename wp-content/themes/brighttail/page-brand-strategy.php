<?php
/*
   Template Name: Brand Strategy   
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="image-container">
               <div id="planet" class="image"></div>
               <div id="satelite" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-9 offset-right-3 col-sm-7 offset-sm-right-5 col-md-6 col-lg-6 col-xl-5">
                     <div class="headlines">
                        <h1><?= get_field('landing_headline'); ?></h1>
                        <p><?= get_field('landing_subheadline'); ?></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <section id="intro">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-7 offset-md-5 col-lg-7 offset-lg-4 col-xl-6 offset-xl-5">
               <div class="headline">
                  <h2 class="case-normal"><?= get_field('intro_headline'); ?></h2>
               </div>
            </div>
            <div class="col col-11 col-sm-10 col-md-7 offset-md-5 col-lg-5 offset-lg-6">
               <div class="text-container">
                  <?= get_field('intro_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="methodology">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-11">
               <div class="headline">
                  <h2><?= get_field('methodology_headline'); ?></h2>
               </div>
            </div>
         </div>  
      </div>
      <div class="subsection"> 
         <div class="container">
            <div class="row justify-content-center">
               <div class="col col-11 col-sm-10 col-md-6 offset-md-6 col-lg-5 offset-right-lg-1 offset-lg-5 offset-right-xl-2">
               <?php if (have_rows('steps')): ?>
                  <ul id="steps" class="bxslider">
                  <?php while (have_rows('steps')): the_row(); ?>
                     <li>
                        <h4><?= get_sub_field('step'); ?></h4>
                        <div class="text-container"><?= get_sub_field('description'); ?></div>
                     </li>
                  <?php endwhile; ?>  
                  </ul>
                  <div id="controls">
                     <div id="cones"></div>
                     <div id="wheels">
                        <span class="front">1</span>
                        <span class="back"></span>
                        <div></div>
                     </div>
                     <label>Step</label>
                  </div>
               <?php endif; ?>
               
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="testimonial">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-9 col-xl-8">
               <h2 class="section-title"><?php the_field('title_testimonial', 'widget_work_section_titles_widget-5'); ?></h2>
               <blockquote><?= get_field('statement'); ?></blockquote>
               <?php if (get_field('customer')): ?>
               <div class="customer">
               
                  <div class="person">
                     <span class="name"><?= get_field('customer')['name'];  ?></span>
                     <span class="title"><?= get_field('customer')['title'];  ?></span>
                  </div>
                  <?php 
                     $company = get_field('customer')['company'];
                     $cta = get_field('cta'); ?>
                  <div class="logo">
                     <div>
                        <img src="<?= (get_field('customer')['option'] ? get_field('customer')['logo']['sizes']['medium'] : get_field('logo', $company->ID)['sizes']['medium']); ?>" title="<?= $company->name; ?>" alt="<?= $company->name; ?>"/>
                     </div>
                  </div>
               </div>
                <a href="<?= get_the_permalink($company->ID); ?>" class="button"><?= $cta; ?></a>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <h2 class="section-title"><?php the_field('contact_headline'); ?></h2>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>