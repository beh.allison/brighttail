jQuery(function($) {
   var $header = $('header'); 
   
   var slides;
   
   $(window).load(listenWidth);
   $(document).load($(window).bind('resize', listenWidth));
   
   $('#primary-menu').append( "<li><a id='to-contact' href='#contact'>Contact</a></li>" );
   
   $('#primary-menu').find('.menu-item-has-children').each(function(i, el) {
      $(el).children('a').after('<span></span>');
   });
   
   $('.menu-item-has-children').children('span').on('click', function() {
      $(this).parent().toggleClass('active');
     
      if ($(this).parent().hasClass('active')) {
         $(this).parent().siblings('.menu-item-has-children').removeClass('active');
         if ($(window).width() < 768) {
            $(this).parent().siblings('.menu-item-has-children').find('.sub-menu').slideUp(600, 'easeOutQuart');
            $(this).next('.sub-menu').slideDown(600, 'easeOutQuart');
         }
        
     } else {
         if ($(window).width() < 768) {
            $(this).next('.sub-menu').slideUp(600, 'easeOutQuart');
         }
     }
   });
   
   $('#primary-menu #to-contact').on('click', function(e) {
      e.preventDefault();
      
      $('#burger-menu').prop('checked', false);
      $header.removeClass('expand');
      
      var section = $(this).attr('href');
         
      $('html, body').animate({
         scrollTop: $(section).offset().top
      }, 1000, 'easeInOutQuart');
   });
   
   $('#burger-menu').change(function() {
      if (this.checked) {
         $header.addClass('expand');
      } else {
         $header.removeClass('expand');
      }
   });
   
   $(window).scroll(function() {
      var  winTop = $(window).scrollTop(),
          screenH = $(window).height();
          
      if (winTop < screenH * 0.05) {
         $header.removeClass('scrolled');
      } else {
         $header.addClass('scrolled');
      }
   });
   
   var $clients_slider;
   
   if ($('body.home').length) {
      var smallStars = [],
          mediumStars = [],
          bigStars = [];
      
      $('#small.stars').css('box-shadow', drawStars(300, smallStars));
      $('#medium.stars').css('box-shadow', drawStars(200, mediumStars));
      $('#big.stars').css('box-shadow', drawStars(60, bigStars));
      
      $clients_slider = $('.clients-slider').bxSlider({
         pager : false,
         pause    : 12000,
         speed    : 900,
         infiniteLoop: false,
         hideControlOnEnd: true,
         easing   : 'easeOutCubic',
         minSlides: slides,
         maxSlides: slides,
         slideWidth: 5000
      });
      
      $('.work').click(function() {
         $(this).addClass('active').siblings().removeClass('active');
      });
   }
   
   if ($('body.single-work').length) {
      $('.screenshots img').click(function() {
         var    i = $(this).parent().index(),
             imgH = $('.overlay .window img').eq(i).height();
             
         if (imgH < $('.overlay').height()) {
            $('.overlay .window').addClass('no-overflow');
         }
         
         $('.overlay .window img').eq(i).show();
         $('.overlay').addClass('active');
         $('body').addClass('no-scroll');
         
      });
      
      $('.overlay .close').click(function() {
         $(this).parent().removeClass('active');
         $('.overlay .window').removeClass('no-overflow');
         $('.overlay .window img').hide();
         $('body').removeClass('no-scroll');
      });
   }
   
   if ($('body.page-template-page-services').length ||
       $('body.page-template-page-inbound-marketing').length ||
       $('body.page-template-page-digital-marketing').length || 
       $('body.error404').length) {
      var smallStars = [],
          mediumStars = [],
          bigStars = [];
      
      $('#small.stars').css('box-shadow', drawStars(360, smallStars));
      $('#medium.stars').css('box-shadow', drawStars(180, mediumStars));
      $('#big.stars').css('box-shadow', drawStars(60, bigStars));
   }
   
   if ($('body.page-template-page-inbound-marketing').length) {
      $('#relationship > li').each(function(i, el) {         
         var waypoint = $(el).waypoint({
            handler: function(direction) {
               $(this[0,'element']).addClass('active');
            },
            offset: '60%'
         });
      });
   }
   
   if ($('body.page-template-page-thought-leadership').length) {
      $('#clients-list').slick({
         centerMode: true,
         centerPadding: '0',
         slidesToShow: 3,
         responsive: [
            {
               breakpoint: 768,
               settings: {
                  centerMode: true,
                  centerPadding: '0',
                  slidesToShow: 1
               }
            }
         ]
      });
      
      var chartAnimChecked = [];
      
      for (var i = 0; i < $('.percentage-chart').length; i++) {
         chartAnimChecked.push(false);
      }
            
      $('.percentage-chart').each(function(i, el) {
         var        $el = $(el),
             percentage = $(el).data('percentage');
         
         var waypoint = $el.waypoint({
            handler: function(direction) {
               $(this[0,'element']).find('.circle').attr( 'stroke-dasharray', percentage + ', 100').css('stroke', '#fff');
               
               if (!chartAnimChecked[i]) {
                  var $number = $el.find('.number');
                  
                  $number.parent().css('opacity', 1);
                  $number.prop('counter',0).animate({
                     counter: $number.text()
                  }, {
                     duration: 1200,
                     easing: 'swing',
                     step: function (now) {
                        $number.text(Math.ceil(now));
                     }
                  });
                  
                  chartAnimChecked[i] = true;
               }
               
            },
            offset: '60%'
         });
         
      });
   }
   
   if ($('body.page-template-page-work').length) {
      $('.work').click(function() {
         $(this).addClass('active').siblings().removeClass('active');
      });
   }
   
   var $steps_slider;
   var wheelDeg = getRotationDegrees($('#wheels')),
       conesDeg = getRotationDegrees($('#cones'));
   
   if ($('#steps').length) {
      $steps_slider = $('#steps').bxSlider({
         mode: 'fade',
         speed: 600,
         adaptiveHeight: true,
         infiniteLoop: false,
         hideControlOnEnd: true,
         pager: false,
         prevSelector: '#controls',
         nextSelector: '#controls',
         prevText: '',
         nextText: '',
         onSlideNext: function($slideElement, oldIndex, newIndex) {
            var $bubbleFront = $('#controls span.front'),
                 $bubbleback = $('#controls span.back');
              
            wheelDeg -= 180;
                             
            $('#wheels').css({'transform': 'rotate(' + wheelDeg + 'deg) translate3d(0, 0, 0)'});
            $bubbleFront.removeClass('front').addClass('back');
            $bubbleback.removeClass('back').addClass('front').html(newIndex + 1);
         },
         onSlidePrev: function($slideElement, oldIndex, newIndex) {
            var $bubbleFront = $('#controls span.front'),
                 $bubbleback = $('#controls span.back');
                 
            wheelDeg += 180;
                             
            $('#wheels').css({'transform': 'rotate(' + wheelDeg + 'deg) translate3d(0, 0, 0)'});
            $bubbleFront.removeClass('front').addClass('back');
            $bubbleback.removeClass('back').addClass('front').html(newIndex + 1);
         }
      });
   }   
   
   function drawStars(num, arr) {
      for (var $i = 0; $i < num; $i++) {
         var shadow = Math.floor(Math.random() * 2000) + 'px ' + Math.floor(Math.random() * 300) + 'px 2px rgba(255,255,255,.3)';
         arr.push(shadow);
      }
      return arr.join();
   }
   
   function getRotationDegrees(obj) {
      var matrix = obj.css("-webkit-transform") || obj.css("-moz-transform") || obj.css("-ms-transform") || obj.css("-o-transform") || obj.css("transform");
      
      if (matrix !== 'none') {
         var values = matrix.split('(')[1].split(')')[0].split(','),
                  a = values[0],
                  b = values[1],
              angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
      } else { 
         var angle = 0;
      }

      if (angle < 0) angle +=360;
      return angle;
   }
   
   function listenWidth() {
      var width = $(window).width();
      
      if (width < 768) {
         $('.menu-item-has-children').each(function(i,el) {
            if ($(el).hasClass('active')) {
               $(el).find('.sub-menu').slideDown(600, 'easeOutQuart');
            } else {
               $(el).find('.sub-menu').slideUp(600, 'easeOutQuart');
            }
         });
      }
      
      if ($('body.home').length) {
         if (width < 576) {
            slides = 2;
         } else if (width >= 576 && width < 992) {
            slides = 3;
         } else {
            slides = 4;
         }
         
         $clients_slider.reloadSlider({
            pager : false,
            pause    : 12000,
            speed    : 900,
            infiniteLoop: false,
            hideControlOnEnd: true,
            easing   : 'easeOutCubic',
            minSlides: slides,
            maxSlides: slides,
            slideWidth: 5000
         });
      }
      
      if ($('#steps').length) {
         if (width < 768) {
            $steps_slider.reloadSlider({
               mode: 'fade',
               speed: 600,
               adaptiveHeight: true,
               infiniteLoop: false,
               hideControlOnEnd: true,
               pager: false,
               prevSelector: '#controls',
               nextSelector: '#controls',
               prevText: '',
               nextText: '',
               onSlideNext: function($slideElement, oldIndex, newIndex) {
                  var $bubbleFront = $('#controls span.front'),
                       $bubbleback = $('#controls span.back');
                    
                  wheelDeg -= 180;
                                   
                  $('#wheels').css({'transform': 'rotate(' + wheelDeg + 'deg) translate3d(0, 0, 0)'});
                  $bubbleFront.removeClass('front').addClass('back');
                  $bubbleback.removeClass('back').addClass('front').html(newIndex + 1);
               },
               onSlidePrev: function($slideElement, oldIndex, newIndex) {
                  var $bubbleFront = $('#controls span.front'),
                       $bubbleback = $('#controls span.back');
                       
                  wheelDeg += 180;
                                   
                  $('#wheels').css({'transform': 'rotate(' + wheelDeg + 'deg) translate3d(0, 0, 0)'});
                  $bubbleFront.removeClass('front').addClass('back');
                  $bubbleback.removeClass('back').addClass('front').html(newIndex + 1);
               }
            });
         } else {
            $steps_slider.reloadSlider({
               mode: 'fade',
               speed: 600,
               adaptiveHeight: true,
               pager: true,
               pagerSelector: '#wheels > div',
               controls: false,
               onSlideBefore: function($slideElement, oldIndex, newIndex) {
                  if (oldIndex > newIndex) {
                     conesDeg -= 180;
                  } else if (oldIndex < newIndex) {
                     conesDeg += 180;
                  }
               
                  $('#cones').css({'transform': 'rotate(' + conesDeg + 'deg) translate3d(0, 0, 0)'});

               }
            });
         }
      }
      
      if ($('body.single-work').length) {
         if ($('.overlay').hasClass('active')) {
            var imgH = $('.overlay .window img:visible').height();
                        
            if (imgH < $('.overlay').height()) {
               $('.overlay .window').addClass('no-overflow');
            } else {
               $('.overlay .window').removeClass('no-overflow');
            }

         }
      }
   }
});