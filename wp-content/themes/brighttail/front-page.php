<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="stars-container">
               <div id="small" class="stars"></div>
               <div id="medium" class="stars"></div>
               <div id="big" class="stars"></div>
            </div>
            <div class="image-container">
               <div id="sun" class="image"></div>
               <div id="vapor" class="image"></div>
               <div id="rocket" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-11 col-sm-9 col-md-6 offset-md-6 col-lg-5">
                     <div class="headlines">
                        <h1><?= get_field('landing_headline'); ?></h1>
                        <p><?= get_field('landing_subheadline'); ?></p>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </div>
   <section id="about">
      <div class="background">
         <div class="content">
            <div class="image"></div>
         </div>
      </div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-8 offset-md-4 col-lg-6 offset-lg-3">
               <div class="headlines">
                  <h2 class="case-normal"><?= get_field('about_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-6 offset-md-6 col-lg-5 col-xl-4">
               <div class="text-container">
                  <?= get_field('about_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="services">
      <div class="background"></div>
      
      <div class="container">
         <div class="image"></div>
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4">
               <div class="headline">
                  <h2><?= get_field('services_headline'); ?></h2>
               </div>
            </div>
            <div class="col col-11 col-sm-9 offset-sm-1 col-md-8 offset-md-0 col-xl-7">
            <?php if (have_rows('services')): ?>
            <ul id="service-list">
               <?php while (have_rows('services')): the_row(); ?>
               <li>
                  <div class="image-container">
                     <img src="<?= get_sub_field('icon')['sizes']['medium']; ?>" title="<?= get_sub_field('service'); ?>" alt="<?= get_sub_field('service'); ?>" />
                  </div><div class="text-container">
                     <h4><?= get_sub_field('service'); ?></h4>
                     <div class="description"><?= get_sub_field('description'); ?></div>
                     <a href="<?= get_sub_field('page_link')['link']; ?>" class="button"><?= get_sub_field('page_link')['cta']; ?></a>
                  </div>
               </li>
               <?php endwhile; ?>
            </ul>
            <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="clients">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-10 offset-1 offset-md-0">
               <div class="headline">
                  <h2><?= get_field('clients_headline'); ?></h2>
               </div>
            </div>
            <div class="col col-11 col-md-10">
               <div class="slider-container">
                  <div class="clients-slider">
                  <?php while ( have_rows('clients') ): the_row(); ?>
                     <div class="slide">
                        <div class="logo-container">
                           <img class="logo" src="<?= get_sub_field('logo')['sizes']['large']; ?>" alt="<?= get_sub_field('client'); ?>" title="<?= get_sub_field('client'); ?>" />
                        </div>
                     </div><?php endwhile; ?>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="case-studies" class="no-skew">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-11 col-lg-3">
               <div class="headline">
                  <h2><?= get_field('work_headline'); ?></h2>
               </div>
            </div>
            <div class="col col-11 col-sm-10 col-md-11 col-lg-8 col-xl-8">
            <?php if (have_rows('work')): ?>
               <div class="works-container">
               <?php while (have_rows('work')): the_row(); 
                  $post = get_sub_field('page_link');
                  $image = get_sub_field('cover_image');
                  setup_postdata($post);
               ?><div class="work">
                     <div>
                        <div class="cover" style="background-image:url(<?= $image['sizes']['large']; ?>);"></div>
                        <div class="title-container">
                           <h3><?php the_title(); ?></h3>
                           <p><?php the_field('headline'); ?></p>
                           <a href="<?php the_permalink(); ?>" class="button">See more</a>
                        </div>
                     </div>
                  </div><?php wp_reset_postdata(); 
                  endwhile; ?>
               </div>
            <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <h2 class="section-title"><?php the_field('contact_headline'); ?></h2>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>
