<?php
/*
   Template Name: Inbound Marketing  
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="stars-container">
               <div id="small" class="stars"></div>
               <div id="medium" class="stars"></div>
               <div id="big" class="stars"></div>
            </div>
            <div class="image-container">
               <div id="moons" class="image"></div>
               <div id="space-probe" class="image"></div>
               <div id="land" class="image"></div>
               <div id="meteor-rain" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-10 offset-1 col-sm-7 offset-sm-4 col-md-6 offset-md-5 col-lg-5 offset-lg-1 offset-lg-right-6 col-xl-4">
                     <div class="headlines">
                        <h1><?= get_field('landing_headline'); ?></h1>
                        <p><?= get_field('landing_subheadline'); ?></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <section id="intro">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-8 offset-md-right-3 col-lg-7 offset-lg-right-4 col-xl-6 offset-xl-right-5">
               <div class="headline">
                  <h2 class="case-normal"><?= get_field('intro_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-8 col-lg-7 col-xl-6">
               <div class="text-container">
                  <?= get_field('intro_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="info">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-8 offset-md-right-3 col-lg-5 offset-lg-right-0">
               <div class="headline">
                  <h2 class="case-normal"><?= get_field('info_headline'); ?></h2>
               </div>
            </div>
            <div class="col col-11 col-sm-10 col-md-9 offset-md-2 col-lg-6 offset-lg-0">
            <?php if (have_rows('relationship')): ?>
               <ul id="relationship">
               <?php while (have_rows('relationship')): the_row(); ?>
                  <li class="<?php if (!get_sub_field('role')['name']) { echo 'no-role'; } ?>">
                     <span class="line"></span>
                     <span class="party"><?= get_sub_field('party'); ?></span>
                  <?php if (get_sub_field('role')['name']): ?>
                     <div class="role">
                        <h4><?= get_sub_field('role')['name']; ?></h4>
                        <p><?= get_sub_field('role')['description']; ?></p>
                     </div>
                  <?php endif; ?>
                  </li>
               <?php endwhile; ?> 
               </ul>   
            <?php endif; ?>
  
            </div>
         </div>
      </div>
   </section>
   <section id="benefits">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-8 offset-md-right-3 col-lg-7 offset-lg-right-4 col-xl-6 offset-xl-right-5">
               <div class="headline">
                  <h2><?= get_field('benefits_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-8 col-lg-7 col-xl-6">
            <?php if (have_rows('benefits')): ?>
               <ul id="benefit-list">
               <?php while (have_rows('benefits')): the_row(); ?>
                  <li><?= get_sub_field('benefit'); ?></li>
               <?php endwhile; ?> 
               </ul>   
            <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="difference">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-8 offset-md-3 col-lg-7 offset-lg-4 col-xl-6 offset-xl-5">
               <div class="headline">
                  <h2><?= get_field('difference_headline'); ?></h2>
               </div>
               <div class="text-container">
                  <?= get_field('difference_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="methodology">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-11">
               <div class="headline">
                  <h2><?= get_field('methodology_headline'); ?></h2>
               </div>
            </div>
         </div>  
      </div>
      <div class="subsection"> 
         <div class="container">
            <div class="row justify-content-center">
               <div class="col col-11 col-sm-10 col-md-6 offset-md-6 col-lg-5 offset-right-lg-1 offset-lg-5 offset-right-xl-2">
               <?php if (have_rows('steps')): ?>
                  <ul id="steps" class="bxslider">
                  <?php while (have_rows('steps')): the_row(); ?>
                     <li>
                        <h4><?= get_sub_field('step'); ?></h4>
                        <div class="text-container"><?= get_sub_field('description'); ?></div>
                     </li>
                  <?php endwhile; ?>  
                  </ul>
                  <div id="controls">
                     <div id="cones"></div>
                     <div id="wheels">
                        <span class="front">1</span>
                        <span class="back"></span>
                        <div></div>
                     </div>
                     <label>Step</label>
                  </div>
               <?php endif; ?>
               
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="testimonial">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-9 col-xl-8">
               <h2 class="section-title"><?php the_field('title_testimonial', 'widget_work_section_titles_widget-5'); ?></h2>
               <blockquote><?= get_field('statement'); ?></blockquote>
               <?php if (get_field('customer')): ?>
               <div class="customer">
                  <div class="person">
                     <span class="name"><?= get_field('customer')['name'];  ?></span>
                     <span class="title"><?= get_field('customer')['title'];  ?></span>
                  </div>
                  <?php 
                     $company = get_field('customer')['company'];
                     $cta = get_field('cta'); ?>
                  <div class="logo">
                     <div>
                        <img src="<?= (get_field('customer')['option'] ? get_field('customer')['logo']['sizes']['medium'] : get_field('logo', $company->ID)['sizes']['medium']); ?>" title="<?= $company->name; ?>" alt="<?= $company->name; ?>"/>
                     </div>
                  </div>
               </div>
                <a href="<?= get_the_permalink($company->ID); ?>" class="button"><?= $cta; ?></a>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 offset-md-right-0 col-xl-3">
               <div class="headlines"><?php the_field('contact_headlines'); ?></div>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>