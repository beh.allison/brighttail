<?php
/*
   Template Name: Digital Marketing  
*/
?>
<?php get_header(); ?>
<div class="wrapper">
   <div class="page-intro">
      <div class="background">
         <div class="content">
            <div class="stars-container">
               <div id="small" class="stars"></div>
               <div id="medium" class="stars"></div>
               <div id="big" class="stars"></div>
            </div>
            <div class="image-container">
               <div id="moon" class="image"></div>
               <div id="satelite" class="image"></div>
            </div>
            <div class="container">
               <div class="row justify-content-center">
                  <div class="col col-10 offset-1 col-sm-8 offset-sm-2 col-md-6 offset-md-4 col-lg-5 offset-lg-2 offset-lg-right-5 col-xl-4 offset-xl-3">
                     <div class="headlines">
                        <h1><?= get_field('landing_headline'); ?></h1>
                        <p><?= get_field('landing_subheadline'); ?></p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <section id="intro">
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-9 offset-md-2 col-lg-8 offset-lg-3 col-xl-7">
               <div class="headline">
                  <h2 class="case-normal"><?= get_field('intro_headline'); ?></h2>
               </div>
            </div>
            <div class="w-100"></div>
            <div class="col col-11 col-sm-10 col-md-7 offset-md-right-2 col-lg-6 offset-lg-right-1 col-xl-5">
               <div class="text-container">
                  <?= get_field('intro_content'); ?>
               </div>
            </div>
         </div>
      </div>
   </section>
   <section id="approach">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-5">
               <div class="headline">
                  <h2><?= get_field('approach_headline'); ?></h2>
               </div>
            </div>
            <div class="col col-11 col-sm-10 col-md-6">
            <?php if (have_rows('approaches')): ?>
               <ul id="approach-list">
                  <?php while (have_rows('approaches')): the_row(); ?>
                  <li>
                     <div class="image-container">
                        <img src="<?= get_sub_field('icon')['sizes']['medium']; ?>" title="<?= get_sub_field('approach'); ?>" alt="<?= get_sub_field('approach'); ?>" />
                     </div><div class="text-container">
                        <h4><?= get_sub_field('approach'); ?></h4>
                        <div class="description"><?= get_sub_field('description'); ?></div>
                      </div>
                  </li>
                  <?php endwhile; ?>
               </ul>
            <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="testimonial">
      <div class="background inverted"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-lg-9 col-xl-8">
               <h2 class="section-title"><?php the_field('title_testimonial', 'widget_work_section_titles_widget-5'); ?></h2>
               <blockquote><?= get_field('statement'); ?></blockquote>
               <?php if (get_field('customer')): ?>
               <div class="customer">
               
                  <div class="person">
                     <span class="name"><?= get_field('customer')['name'];  ?></span>
                     <span class="title"><?= get_field('customer')['title'];  ?></span>
                  </div>
                  <?php 
                     $company = get_field('customer')['company'];
                     $cta = get_field('cta'); ?>
                  <div class="logo">
                     <div>
                        <img src="<?= (get_field('customer')['option'] ? get_field('customer')['logo']['sizes']['medium'] : get_field('logo', $company->ID)['sizes']['medium']); ?>" title="<?= $company->name; ?>" alt="<?= $company->name; ?>"/>
                     </div>
                  </div>
               </div>
                <a href="<?= get_the_permalink($company->ID); ?>" class="button"><?= $cta; ?></a>
               <?php endif; ?>
            </div>
         </div>
      </div>
   </section>
   <section id="contact">
      <div class="background"></div>
      <div class="container">
         <div class="row justify-content-center">
            <div class="col col-11 col-sm-10 col-md-4 col-xl-3">
               <div class="headlines"><?php the_field('contact_headlines'); ?></div>
            </div>  
            <div class="col col-11 col-sm-10 col-md-7 col-xl-8">
               <div class="form-container">
               <?= do_shortcode('[contact-form-7 id="133" title="Contact form general"]'); ?>
               </div>
            </div> 
         </div>
      </div>
   </section>
</div>
<?php get_footer(); ?>